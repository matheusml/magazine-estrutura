(function() {
    'use strict';

    angular
        .module('app')
        .service('HomeService', Home);

    Home.$inject = ['$http', '$q'];

    function Home($http, $q) {
        this.getProducts = function() {
            return $http.get('/products');
        };

        this.getUsers = function(id) {
            var def = $q.defer();

            setTimeout(function() {
                if (id > 300) {
                    return def.resolve(['User1', 'User2']);
                }
                else {
                    return def.reject();
                }
            }, 200);

            return def.promise;
        };
    }
})();
