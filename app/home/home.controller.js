(function() {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', Home);

    Home.$inject = ['HomeService'];

    function Home(HomeService) {
        var vm = this;

        vm.listProducts = listProducts;

        function listProducts() {
            HomeService.getProducts().then(function(response) {

            }).catch(function() {

            });
        }
    }
})();
