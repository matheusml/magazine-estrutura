(function(storage) {
    'use strict';

    angular
        .module('app')
        .factory('Session', Session);

    function Session() {
        var sessionId;

        return {
            create: create,
            destroy: destroy,
            getId: getId,
            isAuthenticated: isAuthenticated
        };

        function create(id) {
            storage.setItem('session', id);
            sessionId = id;
        }

        function destroy() {
            storage.removeItem('session');
            sessionId = null;
        }

        function getId() {
            return sessionId || storage.getItem('session');
        }

        function isAuthenticated() {
            return !!getId();
        }
    }
})(window.localStorage);
