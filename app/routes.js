(function() {
    'use strict';

    angular
        .module('app')
        .config(Config);

    Config.$inject = ['$routeProvider'];

    function Config($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/auth/auth.html',
                controller: 'AuthController',
                controllerAs: 'vm'
            })
            .when('/home', {
                templateUrl: 'app/home/home.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                resolve: {
                    checkRoles: function(RouteAccessService, Profile) {
                        return RouteAccessService.checkRoles(Profile.hasAdminRole() ||
                                                             Profile.hasRootRole());
                    }
                }
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();
