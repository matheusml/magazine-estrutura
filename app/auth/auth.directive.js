(function() {
    'use strict';

    angular
        .module('app')
        .directive('AuthBox', Auth);

    Auth.$inject = ['AuthService'];

    function Auth(AuthService) {
        return {
            restrict: 'E',
            template: '<div ng-if="display">diretiva</div>' +
                      '<input type="submit" ng-click="vm.toggle()">',
            scope: {},
            link: linker,
            controller: controller,
            controllerAs: 'vm'
        };

        function linker($scope, $element, $attrs) {
            //manipular o DOM
            //jquery, etc
        }

        function controller() {
            var vm = this;

            vm.display = false;

            vm.toggle = toggle;

            function toggle() {
                vm.display = !vm.display;
            };
        }
    }
})();
