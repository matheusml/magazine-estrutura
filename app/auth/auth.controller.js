(function() {
    'use strict';

    angular
        .module('app')
        .controller('AuthController', Auth);

    Auth.$inject = ['AuthService', 'Session'];

    function Auth(AuthService, Session) {
        var vm = this;

        vm.login = login;
        vm.logout = logout;

        function login(email, pass) {
            AuthService.login(email, pass).then(function(response) {
                Session.create(response.data.sessionId);
            }).catch(function(response) {
                if (response.status === 401) {
                    Toastr.show('Senha inválida');
                }
                else if (response.status === 403) {
                    Toastr.show('Senha inválida');
                }
                else {
                    Toastr.show('Erro de servidor');
                }
            });
        }

        function logout() {
            Session.destroy();
        }
    }
})();
